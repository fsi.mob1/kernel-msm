Finch-Kernel: kernel_msm mod
============================


## NOTICE

This project is part of [Finch](https://gitlab.com/fsi.mob1/finch); please
see earlier link for entire project description.

We may missed some requirements such as mentioning correct authors of our
dependent library, and so on. We never tries to violate other's, so if you
found mistake please feel free to contact us and we will gladly correct them.

## AUTHORS

Original works from https://android.googlesource.com/kernel/msm.git

See "REAMDE_orig" for original README contents.

## LICENSE
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

See "COPYING" for details.

## CHANGES

Compared to originals we modified files below at 2021-05-17 15:55:29 KST.

- arch/arm64/boot/dts/qcom/sdm845.dtsi
- build.config
- build.config.common
- fs/exec.c
- include/linux/kernel.h
- init/do_mounts.c
- scripts/setlocalversion
- security/security.c
- security/selinux/avc.c
- security/selinux/hooks.c
- security/selinux/ss/services.c
